#### Commands

`git commit -a` : commit to all changes

`ctrl+shift+9` : open Git Tab

#### How to Atom with git
In Terminal:
1. `git clone https://.../asdf.git`

In Atom:
1. File: Add Project folder..
2. Save changes with `ctrl+s`
3. Unstag changes and commit to branch
